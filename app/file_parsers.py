def parse_temperature_data(stream):
    incoming_data = stream.map(lambda line: line.split(","))\
                 .filter(lambda line: len(line)>1)\
                 .map(lambda line: (line[0],line[1]))

    return incoming_data
