from settings import TABLE_NAME
from streamers import CSVStreamer
from analyzers import avg_ten_latest
from file_parsers import parse_temperature_data
from data_movers import write_temperature_data_to_csv
from validators import validate_temperature_data
from transformers import f_to_c_transformer


if __name__ == "__main__":

    streamer = CSVStreamer(file_parser=parse_temperature_data,
                           analyzer=avg_ten_latest,
                           table_name=TABLE_NAME,
                           data_mover=write_temperature_data_to_csv,
                           validator=validate_temperature_data,
                           transformer=f_to_c_transformer
                       )
    streamer.stream()
