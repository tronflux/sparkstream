# set globals here. This really should be an ini file
# that ConfigParser takes for any nontrivial app

INBOUND_DIR = 'file:///opt/sparkstream/inbound'
ERROR_DIR = './errors'
PROCESSED_DIR = './proccessed'
RESULTS_DIR = './results'
DATA_DIR = './data'
TABLE_NAME = 'sensor_data'