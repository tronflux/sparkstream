from settings import ERROR_DIR
from datetime import datetime
from utils import move_to_error

def validator(rdd):
    raw_data = rdd.collect()
    fmt = '%b %d %H:%M:%S %Y'
    for row in raw_data:
        # print(row)
        try:
            datetime.strptime(row[0], fmt)
        except Exception as e:
            print('at least one timestamp entry is invalid')
            move_to_error(raw_data)
        finally:
            pass

        try:
            float(row[1])
        except Exception as e:
            print('at least one entry in sensor data is invalid')
            move_to_error(raw_data)
        finally:
            pass

def validate_temperature_data(dstream):
    if dstream:
        dstream.foreachRDD(validator)