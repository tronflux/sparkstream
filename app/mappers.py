from pyspark.sql import Row


def sensor_csv_mapper(rdd):
    mapped = rdd.map(lambda line: Row(timestamp=str(line[0]), value=float(line[1])))
    return mapped