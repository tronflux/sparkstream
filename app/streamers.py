from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from settings import INBOUND_DIR
from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from mappers import sensor_csv_mapper
from utils import (
    quiet_logs, 
    getSparkSessionInstance
        )


class CSVStreamer(object):
    """see repo for documentation"""
    def __init__(self,
                file_parser,
                analyzer,
                table_name,
                data_mover=None,
                validator=None, 
                transformer=None):
        self.file_parser=file_parser
        self.analyzer=analyzer
        self.table_name=table_name
        self.data_mover=data_mover
        self.validator = validator
        self.transformer = transformer
        # self.files_in_process = None

    def get_data(self, stream):
        return self.file_parser(stream)

    def validate_data(self, stream):
        self.validator(stream)

    def transform_data(self, stream):
        return self.transformer(stream)

    def analyze_data(self, df):
        return self.analyzer(df)

    def move_data(self, results):
        self.data_mover(results)

    def process(self, time, rdd):
        print("========= %s =========" % str(time))

        try:
            spark = getSparkSessionInstance(rdd.context.getConf())

            row_rdd = sensor_csv_mapper(rdd)
            sensor_df = spark.createDataFrame(row_rdd)
            sensor_df.createOrReplaceTempView(self.table_name)

            analyzed = self.analyze_data(sensor_df)

            if self.data_mover:
                self.move_data(analyzed)

        except Exception as e:
            print(e)

    def stream(self):
        sc = SparkContext(appName="CSVStreamer")
        quiet_logs(sc)
        ssc = StreamingContext(sc, 5)
        lines = ssc.textFileStream(INBOUND_DIR)

        parsed_data = self.get_data(lines)

        if self.validator:
            self.validate_data(parsed_data)

        if self.transformer:
            self.transform_data(parsed_data)

        parsed_data.foreachRDD(self.process)

        ssc.start()
        ssc.awaitTermination()
