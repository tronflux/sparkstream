from settings import TABLE_NAME

def avg_ten_latest(df):
    query = '''SELECT AVG(value) as avg_temp 
               FROM 
                   (SELECT * 
                       FROM {0} 
                           ORDER BY timestamp DESC 
                           LIMIT 10) t'''\
            .format(TABLE_NAME)



    return df.sql_ctx.sql(query)


# SELECT AVG(value) as avg_temp FROM (SELECT * FROM sensordata ORDER BY timestamp DESC LIMIT 10) t