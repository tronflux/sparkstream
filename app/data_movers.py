import csv
import json
from datetime import datetime
from settings import RESULTS_DIR


def write_temperature_data_to_csv(results):
    res = results.toJSON()
    row_json = json.loads(res.take(1)[0])
    row = [datetime.strftime(datetime.now(), "%b %d %Y %H:%M:%S"), row_json.pop('avg_temp')]
    with open(RESULTS_DIR + '/streaming_results.csv', 'at+') as file:
        writer = csv.writer(file)
        writer.writerow(row)