# Spark Streaming Test App

# Requirements
- Spark Streaming application should stream new CSV files from a data directory.
- Application should calculate a moving average over the last 10 values from the Values column.
- Application should periodically write this moving average to a file.
- Use the PySpark (Python) API to access Spark.
- Must include the necessary components and be testable within Docker.
- Include documentation for testing


# Example Input CSV

This could be sensor output, for example:

|timestamp  		 |value|	   
|------------------- |-----|
|2017-11-05 00:05:01 |42   |
|2017-11-05 00:05:02 |45   |
|2017-11-05 00:05:03 |38.5 |
|2017-11-05 00:05:04 |40   |


# Design Goals
As usual, finding the sweet spot in the level of abstraction is a challenge undefined by the requirements. I'm choosing a middle-ground here: more flexible than a series of procedural functions, but not going so far up the chain as ABCs. We're not (presumably) building an ETL framework here. My goal is to build a component that could easily be plugged into any framework, whether something off-the-shelf like [airflow](https://airflow.incubator.apache.org/ "airflow") or [luigi](https://github.com/spotify/luigi "luigi") or something custom-built in-house. The app is really just here to demonstrate that it works as intended with Apache Spark.

# Class Behavior
The class needs to handle certain things about the data files we are ingesting. While the Spark Streaming API manages watching the data folder, there are still other tasks the object needs to do --- for example, checking the inbound files for errors, moving them after they've been processed, handing bad files off to the system for logging and review, etc. 

In addition, the class should be flexible enough to handle multiple file formats; normalize, weight, otherwise transform data on-the-fly as needed.

The class writes to a CSV file with a timestamp and the moving average each time a file is streamed.


# N.B.
I'm open to the idea that this class should be perhaps one level higher in abstraction. Perhaps there should be a `FileStreamer` class, of which this is a subclass. And in a situation where we were building a framework, I would go along with the idea of a Streamer class with different providers, but I do think that's really too far for this situation. On the other hand, I could hear arguments saying that this is already too abstract. I'm open to criticism from both approaches.

The goal here is to create a streaming class that is isolated from the data and code that it streams. It really just sets up the Spark Streaming functionality and provides entry points for data, safety (by the validators), transformation, and analysis. My gut tells me that these things should be separate from the mechanical underpinnings. Doing things this way, in my opinion, offers not only flexibility (want to plug in an ML model? Or a Neural Network for some ML stuff? You can do that without touching the infrastructure code!), but it also offers safety. The interface to the class doesn't need to change much at this point, and when it does, it can be versioned, so that people using the class have certain guarantees. And at the same time, data scientists can use this freely without worrying about what it does. They just have to plug in analytics functions. So on the whole, I think I'm in a sweet spot here. But again, I'm open to suggestions and hate mail. :)

# Testing miscellany

This test is going to use temperature data taken from weather sensors. This will be used to demonstrate the flexibility of the program by showing a transformer and a validator. The transformer function will convert sensor data from Farenheit to Centigrade on the fly. The validator will check that each row of the timestamp can be converted to a valid Python datetime object, that each value can be converted to a Python float object, and that the upper and lower bounds are within temperatures that have been experienced on Earth. Failing any aspect of the validation test causes the file to be moved to the error directory and returns control to the streamer to wait for a new file. The error case in the real world could trigger any sort of logging or messaging to let a user know to investigate the problem.

Edit: this was part of the plan, but I went down a rabbit hole and ran out of time. The error-checking is incomplete, and the transformer function is not yet implemented.



# API

# `class CSVStreamer`

# `parameters`

### - `file_parser` --- required
	Accepts a function that defines the way that you expect the csv file to be formatted.

### - `analyzer` --- required
	Accepts a function to process the data after it is received.

### - `table_name` --- required but defined in the settings file here
	Sets the table name for Spark sql registry.

### - `data_mover` --- optional
	Accepts a function that does something to the results after analysis.

### - `validator` --- optional
	Accepts a function that executes tests on the inbound stream and processes errors

### - `transformer` --- optional
    Accepts a function that applies a transformation to the streamed data.

# Testing
1. Clone this repo
2. In your favorite docker environment cd to the local repo directory
3. `docker-compose up`
5. `docker exec -it sparkstream_master_1 /bin/bash`
6. `cd /opt/sparkstream`
7. `/usr/spark-2.2.0/bin/spark-submit app.py`

Once app.py is running, attach a separate terminal window to the docker container with `docker exec -it sparkstream_master_1 /bin/bash` and use `hdfs dfs -copyFromLocal /opt/sparkstream/data/testdata.csv /opt/sparkstream/inbound/test.csv` to atomically toss a file into the stream.


That's all there is to it.



# Acknowledgments

The spark docker files used here were borrowed from [Getty Images](https://github.com/gettyimages/docker-spark "Getty Images") and slightly modified. Many thanks for their generous license.
